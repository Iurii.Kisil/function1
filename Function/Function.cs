﻿using System;

namespace Function
{
    public enum SortOrder { Ascending, Descending, None }
    public static class Function
    {
        
        public static bool IsSorted(int[] array, SortOrder order)
        {
            if (array != null)
            {
                for (int i = 0; i < array.Length - 1; i++)
                {
                    if (order == SortOrder.Ascending)
                    {
                        if (array[i] > array[i + 1])
                        {
                            return false;
                        }

                    }
                    else
                    {
                        if (array[i] < array[i + 1])
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            return false;
        }
       
        public static void Transform(int[] array, SortOrder order)
        {
            
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
            if (array != null)
            {
                if (IsSorted(array, order))
                {
                    for (int i = 0; i < array.Length; i++)
                    {
                        array[i] += i;
                    }
                }

            }
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
            Console.WriteLine("----------------------------");
        }

       
        public static double MultArithmeticElements(double a, double t, int n)
        {
            double element = a, multiply = 1;  //was 1

            for (int i = 0; i < n; i++)
            {
                multiply *= element;
                element += t;

            }
            return multiply;
        }


       
        public static double SumGeometricElements(double a, double t, double alim)
        {
            if (a > 0)
            {
                double element = a, sum = 0;
                while (element > alim)
                {
                    sum -= element;
                    element += t;
                }
                return sum;
            }
            return 0;

        }



    }
}
